from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0002_auto_20180610_2129'),
    ]

    operations = [
        migrations.DeleteModel(
           name='eventrun',
        ),
    ]