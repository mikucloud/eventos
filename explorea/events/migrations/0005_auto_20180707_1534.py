# Generated by Django 2.0.5 on 2018-07-07 13:34

from django.conf import settings
from django.db import migrations, models

from django.utils.text import slugify

def to_slug(apps, schema_editor):

    Event = apps.get_model('events', 'Event')
    for event in Event.objects.all():
        event.slug = slugify(event.name + '-with-' + event.host.username)
        event.save()

class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('events', '0004_recreate_event_run'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='event',
            options={'ordering': ['name']},
        ),
        migrations.AlterModelOptions(
            name='eventrun',
            options={'ordering': ['date', 'time']},
        ),
        migrations.AddField(
            model_name='event',
            name='created',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
        migrations.AddField(
            model_name='event',
            name='slug',
            field=models.SlugField(max_length=200, null=True, unique=True),
        ),
        migrations.AlterField(
            model_name='event',
            name='category',
            field=models.CharField(choices=[('FN', 'fun'), ('RX', 'relax'), ('EX', 'experience'), ('SI', 'sights')], default='FN', max_length=20),
        ),
        migrations.AlterUniqueTogether(
            name='event',
            unique_together={('name', 'host')},
        ),

        migrations.RunPython(to_slug),
        migrations.AlterField(
            model_name='event',
            name='slug',
            field=models.SlugField(null=False, max_length=200, unique=True),
        ),
    ]
