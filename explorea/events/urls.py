from django.urls import path

from . import views

app_name='events'

urlpatterns = [
    
    
    #path('', views.event_listing, name='events'),
    
    #path('new/', views.create_event, name='create_event'),
    path('new/', views.CreateEventView.as_view(), name='create_event'),
    #path('my_events/', views.my_events, name='my events'),
    path('my_events/', views.MyEventsView.as_view(), name='my_events'),
    #path('search/', views.event_search, name='search'),
    path('search/<category>/', views.EventListAndSearchView.as_view(), name='search'),

    #path('events/detail/<slug:slug>/', views.event_detail, name='detail'),
    path('events/detail/<slug:slug>/', views.EventDetailView.as_view(), name='detail'),
    #path('events/edit/<slug:slug>/', views.edit_event, name='edit_event'),
    path('events/edit/<slug:slug>/', views.UpdateEventView.as_view(), name='edit_event'),
    
    #path('events/delete/<slug:slug>/', views.delete_event, name='delete_event'),
    path('events/delete/<slug:slug>/', views.DeleteEventView.as_view(), name='delete_event'),
    

    path('events_run/', views.events_run_listing, name='events_run'),
    #path('events_run/new/<slug:slug>', views.new_event_run, name='new_event_run'),
    path('events_run/new/<slug:slug>', views.CreateEventRunView.as_view(), name='new_event_run'),
    #path('events_run/edit/<int:event_run_id>', views.edit_event_run, name='edit_event_run'),
    path('events_run/edit/<int:event_run_id>', views.UpdateEventRunView.as_view(), name='edit_event_run'),
    #path('events_run/delete/<int:event_run_id>', views.delete_event_run, name='delete_event_run'),
    path('events_run/delete/<int:pk>', views.DeleteEventRunView.as_view(), name='delete_event_run'),

    #path('<category>/', views.event_listing, name='events'),
    #path('<category>/', views.EventListView.as_view(), name='events'),

    
]