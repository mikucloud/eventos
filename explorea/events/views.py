from django.shortcuts import render,redirect
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
import datetime
from django.db.models import Min
from django.urls import reverse_lazy
from django.contrib import messages


import pdb; 

#class views imported

from django.views.generic import ListView, DetailView,TemplateView
from django.views.generic.edit import DeleteView, CreateView, UpdateView, FormView, FormMixin

#class views mixins
from django.contrib.auth.mixins import LoginRequiredMixin
#from django.contrib.auth import get_user_model


from .models import Event, EventRun, EventManager,Album, Image
from .forms import EventForm,EventRunForm,EventFilterForm,EventSearchFilterForm
from explorea.cart.forms import CartAddForm

# Create your views here.

class GetFormMixin(FormMixin):

    def get_form_kwargs(self):
        kwargs = {
            'initial': self.get_initial(),
            'prefix': self.get_prefix(),
            'data': self.request.GET,
        }
        return kwargs

class MessageActionMixin:
    
    @property
    def success_message(self):
        return NotImplemented

    @property
    def error_message(self):
        return NotImplemented

    def get_object(self, queryset=None):
        #pdb.set_trace()
        return getattr(self, 'object', None) or super().get_object(queryset)

    def form_valid(self, form):
        #pdb.set_trace()
        obj = self.get_object()
        messages.success(self.request, self.success_message % obj.__dict__)

    def form_invalid(self, form):
        #pdb.set_trace()
        obj = self.get_object()
        messages.error(self.request, self.error_message % obj.__dict__)

    def delete(self, request, *args, **kwargs):
        obj = self.get_object()
        messages.success(self.request, self.success_message % obj.__dict__)
        return super().delete(request, *args, **kwargs)

class IndexView(TemplateView):
    template_name = 'events/index.html'

class EventListAndSearchView(GetFormMixin,ListView):
    model = EventRun
    form_class = EventSearchFilterForm
    template_name = 'events/event_listing.html'
    paginate_by = 4
    context_object_name = 'event_runs'
    

    def get(self, request, *args, **kwargs):      
        self.form = self.get_form()
        if self.form.is_valid():
            self.query = self.form.cleaned_data.pop('q')
            return super().get(request, *args, **kwargs)
        else:
            self.object_list = []
            return self.form_invalid(self.form)

    def get_queryset(self):
        
        qs =  self.model._default_manager.search(self.query)
        qs = qs.filter_by_category(self.kwargs['category'])
        return qs.filter_first_available(**self.form.cleaned_data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filter_form'] = self.form
        return context

# class EventListView(GetFormMixin, ListView):
    
#     model = EventRun
#     context_object_name = 'event_runs'
#     form_class = EventFilterForm
#     template_name = 'events/event_listing.html'
#     paginate_by = 4
    
#     def get(self, request, *args, **kwargs):
#         self.form = self.get_form()
#         if self.form.is_valid():
#             return super().get(request, *args, **kwargs)
#         else:
#             self.object_list = []
#             return self.form_invalid(self.form)
        

#     def get_queryset(self):
#         qs = self.model._default_manager.all().filter_by_category(self.kwargs['category'])
#         return qs.filter_first_available(**self.form.cleaned_data)

#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context['filter_form'] = self.form
#         return context       

class EventDetailView(DetailView):
    model = Event
    template_name = 'events/event_detail.html'
    context_object_name = 'events'
    form_class = CartAddForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['events_run'] = self.object.active_runs()
        context['cart_add_form'] = self.form_class()
        return context

class CreateEventView(MessageActionMixin, CreateView):
    model = Event
    form_class = EventForm
    template_name = 'events/create_event.html'
    success_message = 'The event %(name)s has been created successfully'
    error_message = 'The event could not be created'

    def form_valid(self, form):
        form.cleaned_data.pop('gallery')
        event =self.object =  Event.objects.create(host=self.request.user, **form.cleaned_data)
       

        # save the individual images
        for file in self.request.FILES.getlist('gallery'):
            Image.objects.create(album=event.album, image=file, title=file.name)

        super().form_valid(form)

        return redirect(event.get_absolute_url())

class UpdateEventView(MessageActionMixin, UpdateView):
    model = Event
    form_class = EventForm
    template_name = 'events/create_event.html'
    success_message = 'The event %(name)s has been created successfully'
    error_message = 'The event %(name)s could not be updated'

    def form_valid(self, form):
        event = form.save()
        
        # save the individual images
        for file in self.request.FILES.getlist('gallery'):
            Image.objects.create(album=event.album, image=file, title=file.name)

        super().form_valid(form)

        return redirect(event.get_absolute_url())
    


class DeleteEventView(MessageActionMixin,DeleteView):
    model = Event
    success_url = reverse_lazy('events:my_events')
    success_message = 'The event %(name)s has been created successfully'
    error_message = 'The event %(name)s could not be updated'

class MyEventsView(ListView):
    context_object_name = 'my_events'
    template_name = 'events/my_events.html'

    def get_queryset(self):
        return Event.objects.filter(host_id=self.request.user.id)



class CreateEventRunView(MessageActionMixin, CreateView):
    model = EventRun
    form_class = EventRunForm
    template_name = 'events/new_event_run.html'
    success_message = 'The event run has been created successfully'
    error_message = 'The event run for could not be created'
    

    def form_valid(self, form):
        slug=self.kwargs['slug']
        event=Event.objects.get(slug=slug)
        
        self.object = EventRun.objects.create(event=event, **form.cleaned_data)
        
        super().form_valid(form)
        
        return redirect(self.object.event.get_absolute_url())
    
    def form_invalid(self, form):
        return self.render_to_response(self.get_context_data(form=form))


class UpdateEventRunView(MessageActionMixin, UpdateView):
    model = EventRun
    form_class = EventRunForm
    template_name = 'events/new_event_run.html'
    pk_url_kwarg = 'event_run_id' 
    success_message = 'The event run has been updated successfully'
    error_message = 'The event run for could not be updated'
    

    def get_success_url(self):
        return self.object.event.get_absolute_url()

    def form_valid(self, form):
        # pdb.set_trace()
        self.object = form.save()
        # pdb.set_trace()
        # self.__dict__
        #pdb.set_trace()
        super().form_valid(form)
        #pdb.set_trace()
        
        return redirect(self.object.event.get_absolute_url())

    def form_invalid(self, form):
        return self.render_to_response(self.get_context_data(form=form))
        

class DeleteEventRunView(MessageActionMixin,DeleteView):
    model = EventRun
    #success_url = reverse_lazy(self.object.event.get_absolute_url())
    success_message = 'The event run has been deleted successfully'
    error_message = 'The event run for could not be deleted'

    def get_success_url(self):
        
        return self.object.event.get_absolute_url()

def events_run_listing(request):
   
    events_run=EventRun.objects.all()
    return render(request,'events/events_run_listing.html',{'events_run': events_run})