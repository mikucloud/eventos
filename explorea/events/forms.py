from django import forms
from django.core.exceptions import ValidationError
import datetime

from explorea.events.models import Event,EventRun
from core.validators import validate_future_date

class EventForm(forms.ModelForm):
    gallery = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}), required=False)

    class Meta:
        model = Event
        exclude = ['host','slug']
           

class EventRunForm(forms.ModelForm):
    
    date = forms.DateField(input_formats=["%d.%m.%Y"], 
        widget=forms.DateInput(format = '%d.%m.%Y'),
        error_messages={'invalid':'Uncorrect format (correct is dd.mm.yyyy) or date in past is entered'},
        validators=[validate_future_date])
    time = forms.TimeField(error_messages={'invalid':'Correct format is hh:mm:ss or hh:mm'})
    

    class Meta:
        model = EventRun
        exclude = ['event']

class EventFilterForm(forms.Form):             

    ORDERING_CHOICES = (
        ('price', 'cheapest',), 
        ('-price', 'most expansive',),
        ('-seats_available', 'seats avail.',),
        ('date', 'date',),
        ('event.name', 'event name',),
        ('event.host.username', 'host name',),
        )
     
    date_from = forms.DateField(label="From", initial=None,
        widget=forms.SelectDateWidget, required=False,
        )
     
    date_to = forms.DateField(label="To", initial=None,
        widget=forms.SelectDateWidget, required=False,
        )
        
    guests = forms.IntegerField(required=False, min_value=1)

    order_by= forms.ChoiceField(label="Order by",initial=None,
         widget=forms.RadioSelect, choices=ORDERING_CHOICES,required=False)

    # PRICE_ASC = 'price'
    # PRICE_DESC = '-price'
    # NUM_OF_PLACES = 'seats_available'
    # DATE = 'date'
    # EVENT_NAME = 'event__name'
    # HOST_NAME = 'event__host__username'       
        
    def clean(self):
            clean_data=super(EventFilterForm,self).clean()
            date_from = self.cleaned_data.get("date_from")
            date_to = self.cleaned_data.get("date_to")

            if date_from and date_to:
                if date_from > date_to:
        
                    msg="Date from must be before date to"
                    self.add_error('date_from', msg)
                
            actual_date=datetime.datetime.now().date()

            if date_from:
                if actual_date >= date_from:
                    msg="Only future dates are possible..."
                    self.add_error('date_from', msg)

            if date_to:        
                if actual_date >= date_to:
                    msg="Only future dates are possible..."
                    self.add_error('date_to', msg)

class EventSearchFilterForm(EventFilterForm):
    
     q = forms.CharField(required=False, max_length=1000, initial='',
                                    widget=forms.HiddenInput())
    