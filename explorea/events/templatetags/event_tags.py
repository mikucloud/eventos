from django import template
from django.utils import timezone
import datetime


register = template.Library()

@register.filter
def min_attr_value(objects, field_name):
    values = [getattr(obj,field_name) for obj in objects]
    if values:
        return min(values)

@register.filter
def max_attr_value(objects, field_name):
    values = [getattr(obj,field_name) for obj in objects]
    if values:
        return max(values)

@register.filter
def active(objects,field_name="date"):
    active_event_runs = [obj for obj in objects if getattr(obj,field_name)>=timezone.now().date()]
    if active_event_runs:
        return active_event_runs

@register.filter
def has(obj, attr_name):
    try:
        return bool(getattr(obj, attr_name))
    except ValueError:
        return False
#return minimal object as a queryset of given queryset. Ordered by given attr.
# @register.filter
# def min_attr_queryset(objects, field_name):
#     if objects:
#         objects_ordered = sorted(objects,key=lambda order_by:getattr(order_by,field_name))
#         min_object=objects_ordered[0]
#         return min_object

#Return given attr of given object(queryset)
# @register.filter
# def get_attr_value(object, field_name):
#     if object:
#         value = getattr(object,field_name)
#         if value:
#             return value




# @register.filter
# def next_date(objects,field_name="date"):
#     dates = sorted(objects.values_list(field_name, flat=True))
#     for dt in dates:
#         if dt >= timezone.now().date():
#             return dt





