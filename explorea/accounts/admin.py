from django.contrib import admin

# Register your models here.

from .models import Profile

admin.site.site_header = 'Explorea Admin'

@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    pass