from django.shortcuts import render, redirect
from django.contrib.auth import get_user_model, authenticate, login,update_session_auth_hash
from .forms import RegisterForm, EditUserForm, EditProfileForm
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.decorators import login_required
from .models import Profile
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.utils.http import urlsafe_base64_decode
from django.contrib.sites.shortcuts import get_current_site
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes
from django.contrib.auth import get_user_model
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect
import pdb

from django.views.generic import ListView, DetailView,TemplateView
from django.views.generic.edit import DeleteView, CreateView, UpdateView, FormView, FormMixin

#class views mixins
from django.contrib.auth.mixins import LoginRequiredMixin
#from django.contrib.auth import get_user_model


token_generator = PasswordResetTokenGenerator()
UserModel = get_user_model()



# Create your views here.

class ProfileView(TemplateView):
    template_name = 'accounts/profile.html'


class RegisterView(CreateView):
    model = UserModel
    form_class = RegisterForm
    template_name = 'accounts/register.html'

    def form_valid(self, form):
        user=form.save()

        raw_password = form.cleaned_data.get('password1')  

        user = authenticate(username=user.username, password=raw_password)
        login(self.request, user)

        return redirect('accounts:profile')

# class EditProfileView(LoginRequiredMixin, FormView):
#     template_name ='accounts/edit_profile.html'
#     success_url = reverse_lazy('accounts:profile')
    
#     def get(self, request, *args, **kwargs):
#         user_form = EditUserForm(request.POST or None, instance=request.user)
#         profile_form = EditProfileForm(request.POST or None, request.FILES or None, instance=request.user.profile)

#         kwargs={'user_form': user_form,'profile_form': profile_form }
#         return self.render_to_response(self.get_context_data(**kwargs))

#     def get_context_data(self, **kwargs):
#         return kwargs    
    
#     def post(self, request, *args, **kwargs):
#         user_form = EditUserForm(request.POST or None, instance=request.user)
#         profile_form = EditProfileForm(request.POST or None, request.FILES or None, instance=request.user.profile)

#         if user_form.is_valid() and profile_form.is_valid():
#             user = user_form.save()
#             profile = profile_form.save()
#             return HttpResponseRedirect(self.get_success_url())
#         else:
#             kwargs={'user_form': user_form,'profile_form': profile_form }
#             return self.render_to_response(self.get_context_data(**kwargs))

class EditProfileView(LoginRequiredMixin, FormView):
    template_name ='accounts/edit_profile.html'
    success_url = reverse_lazy('accounts:profile')
    user_form=None
    profile_form=None
    
    def dispatch(self, request, *args, **kwargs):
        self.user_form = EditUserForm(request.POST or None, 
                                instance=request.user)
        self.profile_form = EditProfileForm(request.POST or None, 
                                        request.FILES or None, 
                                        instance=request.user.profile)
        self.form_context = {'user_form': self.user_form,'profile_form': self.profile_form }

        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        return self.render_to_response(self.form_context)
 
    
    def post(self, request, *args, **kwargs):
        if self.user_form.is_valid() and self.profile_form.is_valid():
            user = self.user_form.save()
            profile = self.profile_form.save()
            return HttpResponseRedirect(self.get_success_url())
        else:
            return self.render_to_response(self.form_context)
            


class ChangePasswordView(LoginRequiredMixin, UpdateView):
    model = UserModel
    form_class = PasswordChangeForm
    template_name = 'accounts/change_password.html'
    success_url = reverse_lazy('accounts:profile')

    def form_valid(self, form):
        self.object = form.save()
        update_session_auth_hash(self.request, self.object)
        return HttpResponseRedirect(self.get_success_url())

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.pop('instance')
        kwargs.update({'user': self.object})
        return kwargs

    
    def get_object(self):
        return self.request.user

class HostListView(LoginRequiredMixin,ListView):
    context_object_name = 'profiles'
    template_name = 'accounts/host_list.html'

    def get_queryset(self):
        profiles = Profile.objects.filter(is_host=True)
        if self.request.user.profile.is_host:
            profiles = profiles.exclude(user__pk=self.request.user.id)
        return profiles


class BecomeHostView(LoginRequiredMixin,TemplateView):
    template_name = 'accounts/verification_sent.html'

    def get(self, request, *args, **kwargs):
        verification_email_template = 'accounts/host_verification_email.html'
        email_context = {
        'user': request.user,
        'domain': get_current_site(request).domain,
        'uidb64': urlsafe_base64_encode(force_bytes(request.user.pk)).decode(),
        'token': token_generator.make_token(request.user)
        }

        html_body = render_to_string(verification_email_template, email_context)
        subject = 'Explorea Host Verification'
        from_email = 'admin@explorea.com'
        to_email = request.user.email
        
        email = EmailMessage(subject, html_body, from_email, [to_email])
        email.send()

        return super().get(self, request,*args,**kwargs)



# def activate_host(request, uidb64, token):
#     try:
#         uid = urlsafe_base64_decode(uidb64).decode()
#         user = UserModel._default_manager.get(pk=uid)
#     except (TypeError, ValueError, OverflowError, User.DoesNotExist):
#         user = None

#     if user is not None and token_generator.check_token(user, token):
#         user.profile.is_host = True
#         user.profile.save()
#         return render(request, 'accounts/verification_complete.html')

#     else:
#         return render(request, 'accounts/invalid_link.html')

class ActivateHost(TemplateView):
    def get(self, request, *args, **kwargs):
        uidb64=self.kwargs['uidb64']
        token=self.kwargs['token']
        try:
            uid = urlsafe_base64_decode(uidb64).decode()
            user = UserModel._default_manager.get(pk=uid)
        except (TypeError, ValueError, OverflowError, User.DoesNotExist):
            user = None

        if user is not None and token_generator.check_token(user, token):
            user.profile.is_host = True
            user.profile.save()
            self.template_name = 'accounts/verification_complete.html'

        else:
            self.template_name = 'accounts/invalid_link.html'
        
        return super().get(self, request,*args,**kwargs)

class HostProfileView(LoginRequiredMixin,DetailView):
    model = UserModel
    context_object_name = 'host'
    template_name = 'accounts/host_profile.html'
    slug_field = 'username'
    slug_url_kwarg = 'username'


    