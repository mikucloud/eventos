class VerificationTokenGenerator (PasswordResetTokenGenerator):
    """ Overriden check_token method to use mz own settings variable VERIFICATION_TOKEN_TIMEOUT_DAYS"""

    def check_token(self, user, token):
            """
            Check that a password reset token is correct for a given user.
            """
            if not (user and token):
                return False
            # Parse the token
            try:
                ts_b36, hash = token.split("-")
            except ValueError:
                return False

            try:
                ts = base36_to_int(ts_b36)
            except ValueError:
                return False

            # Check that the timestamp/uid has not been tampered with
            if not constant_time_compare(self._make_token_with_timestamp(user, ts), token):
                return False

            # Check the timestamp is within limit
            if (self._num_days(self._today()) - ts) > settings.VERIFICATION_TOKEN_TIMEOUT_DAYS:
                return False

            return True