from django.conf import settings
from django.shortcuts import redirect

from .cart import Cart

class CartDetailViewMiddleware:

    def __init__(self, get_response):
        self.get_response = get_response
    
    def __call__(self, request):
        response = self.get_response(request)
        return response

    def process_view(self, request, view_func, view_args, view_kwargs):
        request.cart = Cart(request.session)
        #print(request.GET['next'])

    def process_exception(self, request, exception):
        print('process_exception')

    def process_template_response(self, request, response):
        items_numebrs=0
        for item in request.cart:
            items_numebrs+=1
        #print(items_numebrs)  
        if not request.cart.is_empty():
            response.context_data['cart'] = request.cart
        response.context_data['items_number'] = items_numebrs
        
        return response
        