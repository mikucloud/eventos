from django.test import TestCase
from django.core.exceptions import ValidationError


from ..forms import CartAddForm, PositiveIntegerField



class PositiveIntegerFieldTest(TestCase):

    def test_validate_positive_integer(self):
        field = PositiveIntegerField()
        self.assertIsNone(field.validate(2))

    def test_validated_integer_less_than_one(self):
        field = PositiveIntegerField()
        with self.assertRaises(ValidationError):
            field.validate(-5)


class CartAddFormTest(TestCase):

    def test_form_valid(self):
        data = {'quantity': 1, 'product_id': 20}
        form = CartAddForm(data=data)
        
        self.assertTrue(form.is_valid())
        

    def test_form_invalid_with_less_than_one(self):
        data = {'quantity': 0, 'product_id': 21}
        form = CartAddForm(data=data)

        self.assertFalse(form.is_valid())
        self.assertIsNotNone(form.errors.get('quantity'))
        self.assertIn('Only positive integer allowed', form.errors.get('quantity'))
    
    def test_form_invalid_with_quantity_more_than_seats_available(self):
        
        data = {'quantity': 5, 'product_id': 22,'max_seats':4}
        form = CartAddForm(data=data)
        #max_seats=cls.eventrun.seats_available
        
        self.assertFalse(form.is_valid())
        self.assertIsNotNone(form.errors.get('quantity'))
        self.assertIn("It is not enough seats available", form.errors.get('quantity'))