from django.test import TestCase
from django.urls import resolve, reverse
from unittest import skip
from django.contrib.auth import get_user_model
from datetime import date, timedelta, time

from explorea.events.models import Event, EventRun
from ..views import CartDetailView, CartAddView
from ..forms import CartAddForm

UserModel = get_user_model()

class CartDetailViewTest(TestCase):
    @classmethod
    def setUpClass(cls):
        # fix login
        super().setUpClass()
        cls.username = 'TEST'
        cls.password = 'password123'
        cls.user = UserModel.objects.create_user(username=cls.username, 
                                                 password=cls.password)
        # allow showing cart detail
        cls.event = Event.objects.create(host=cls.user, 
                                         name='Test Event1', 
                                         description='Test Event1 Description', 
                                         location='Test Event1 Location')
        cls.eventrun = EventRun.objects.create(event=cls.event,
                                               date=date.today() + timedelta(days=2),
                                               time=time(hour=10, minute=30),
                                               seats_available=20,
                                               price=200)

    def setUp(self):
        self.client.login(username=self.username, password=self.password)

    def test_cart_detail_url_resolves_to_cart_detail_view(self):
        page = resolve(reverse('cart:detail'))
        self.assertEqual(page.func.__name__, CartDetailView.__name__)

    def test_cart_detail_shows_no_items_when_empty(self):
        response = self.client.get(reverse('cart:detail'))
        self.assertIn(b'The cart is empty', response.content)

    # def test_cart_detail_shows_items_when_not_emtpy(self):
    #     test_data={'pruduct_id':self.event.id,
    #                 'quantity':3,
    #                 'max_seats':6
    #                 }
    #     #import pdb; pdb.set_trace()
    #     self.client.post(
    #             reverse('cart:add', args=[self.event.slug]),test_data
    #         )
    #     #import pdb; pdb.set_trace()
    #     response = self.client.get(reverse('cart:detail'))
        
        
    #     self.assertIn(b'Test Event', response.content)
    #     self.assertIn(b'quantity: 3', response.content)
        


        

class CartAddViewTest(TestCase):

    @classmethod
    def setUpClass(cls):
        # fix login
        super().setUpClass()
        cls.username = 'TEST'
        cls.password = 'password123'
        cls.user = UserModel.objects.create_user(username=cls.username, 
                                                 password=cls.password)
        
        cls.event = Event.objects.create(host=cls.user, 
                                         name='Test Event', 
                                         description='Test Event Description', 
                                         location='Test Event Location')
        cls.eventrun = EventRun.objects.create(event=cls.event,
                                               date=date.today() + timedelta(days=2),
                                               time=time(hour=10, minute=30),
                                               seats_available=20,
                                               price=200)
    def setUp(self):
        self.view = CartAddView
        self.url = reverse('cart:add', args=[self.event.slug])
        self.client.login(username=self.username, password=self.password)

    def test_cart_detail_url_resolves_to_cart_detail_view(self):
        page = resolve(self.url)
        self.assertEqual(page.func.__name__, self.view.__name__)
   
    def test_cart_add_view_valid_form_data_redirects_to_corresponding_product_page_with_success(self):
        response = self.client.post(self.url, 
                                   data={'quantity': 1, 'product_id': 1}, 
                                   follow=True)
        self.assertRedirects(response, self.event.get_absolute_url())

        msg = list(response.context.get('messages'))[0]
        self.assertEqual(msg.message, 'The run has been added to the cart')
        self.assertEqual(msg.tags, 'success')

    def test_cart_add_view_invalid_form_data_redirects_to_corresponding_product_page_with_error(self):
        response = self.client.post(self.url, 
                                   data={'quantity': 0, 'product_id': 1}, 
                                   follow=True)
        
        self.assertRedirects(response, self.event.get_absolute_url())

        msg = list(response.context.get('messages'))[0]
        self.assertEqual(msg.message, 'The run could not be added into the cart')
        self.assertEqual(msg.tags, 'error')