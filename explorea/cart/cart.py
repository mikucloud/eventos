from django.conf import settings
import json

from explorea.events.models import EventRun

class Cart:
    def __init__(self, session):
        self.session = session
        self.cart = session.get(settings.CART_SESSION_ID)
        if not self.cart:
            self.cart = self.session[settings.CART_SESSION_ID] = {}

    def __getitem__(self, item):
        return self.cart[str(item)]

    def __contains__(self, item):
        return item in self.cart
    
    def __iter__(self):
        for item in self.cart:
            run=EventRun.objects.filter(pk=item)[0]
            self.cart[item].update({'run':run})
            yield self.cart[item]

    def is_empty(self):
        return len(self.cart) == 0

    def get(self, product_id):
        try:
            return self.cart[str(product_id)]['quantity']
        except KeyError:
            return 0

    def add(self, product_id, quantity=1,**kwargs):
        #run=EventRun.objects.filter(pk=product_id)
        self.cart.update({str(product_id): {'quantity':quantity}})
        self.session[settings.CART_SESSION_ID] = self.cart
        self.session.modified = True  

    def remove(self, product_id):
        if product_id in self.cart:
            del self.cart[str(product_id)]
        
        self.session[settings.CART_SESSION_ID] = self.cart
        self.session.modified = True

    
    
    