from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views.generic import TemplateView, FormView
from django.views.generic.detail import SingleObjectMixin
from django.contrib import messages

from explorea.events.models import Event, EventRun
from .forms import CartAddForm
from .cart import Cart

class CartDetailView(TemplateView):
    template_name = "cart/cart_detail.html"

    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     cart = Cart(self.request.session)
    #     if cart.is_empty():
    #         context = None
    #     else:
    #         context['cart'] = cart
    #     return context
        
        

class CartAddView(SingleObjectMixin, FormView):
    model = Event
    form_class =  CartAddForm
    template_name = 'events/event_detail.html'
    slug_url_kwarg = 'event_slug'

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().post(request, *args, **kwargs)

    def get_success_url(self):
        return self.object.get_absolute_url()

    def form_valid(self, form):
        cart = Cart(self.request.session)
        cart.add(**form.cleaned_data)
        messages.success(self.request, 'The run has been added to the cart')
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form):
        messages.error(self.request, 'The run could not be added into the cart')
        return HttpResponseRedirect(self.get_success_url())


class RemoveCartView(TemplateView):
    template_name = "cart/cart_detail.html"
    pk_url_kwarg = 'product_id'


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        cart = Cart(self.request.session)
        cart.remove(kwargs['product_id'])
        if cart.is_empty():
            context = None
        else:
            context['cart'] = cart
        return context
