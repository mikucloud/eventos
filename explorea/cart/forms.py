from explorea.events.models import Event, EventRun
from django import forms


class PositiveIntegerField(forms.IntegerField):

    def validate(self, value):
        super().validate(value)
        if value < 1:
            raise forms.ValidationError('Only positive integer allowed')

class CartAddForm(forms.Form):
    quantity = PositiveIntegerField(initial=0)
    product_id = forms.CharField(max_length=10, widget=forms.HiddenInput())
    max_seats = forms.IntegerField(required=False, initial=0, widget=forms.HiddenInput)
                                   

    def clean(self):
            
            super().clean()
            
            product_id = self.cleaned_data.get("product_id")
            quantity = self.cleaned_data.get("quantity")
            max_seats = self.cleaned_data.get("max_seats")
            
            if quantity is not None and max_seats is not None:
                if quantity > max_seats:
                        msg="It is not enough seats available"
                        self.add_error('quantity', msg)
                        print(self.has_error('quantity'))
                        
                        
                
            