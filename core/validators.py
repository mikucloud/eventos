import re
import datetime
from django.core.exceptions import ValidationError

def validate_human_name(value):

    regex = r'^[A-Za-z\s\-]+$'
    if not re.match(regex, value):
        raise ValidationError(
            'Names can contain only alpha characters',
            code='invalid')

def validate_future_date(date):

    actual_date=datetime.datetime.now().date()

    if actual_date >= date:
        raise ValidationError(
            'Please respect the time unidirectionality',
            code='invalid')

